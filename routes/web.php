<?php

Route::get('/', function () {
    return view('welcome');
});
Route::get('/admin/home', 'Admin\HomeController@index');
Route::get('/admin/gejala/', 'Admin\GejalaController@index');

// Route::resource('/admin/penyakit', 'Admin\PenyakitController');

// route penyakit
Route::get('/admin/penyakit/', 'Admin\PenyakitController@index');
Route::get('/admin/penyakit/create', 'Admin\PenyakitController@create');
Route::post('/admin/penyakit', 'Admin\PenyakitController@store');
Route::get('/admin/penyakit/{kd_penyakit}/edit', 'Admin\PenyakitController@edit');
Route::put('/admin/penyakit/{kd_penyakit}', 'Admin\PenyakitController@update');
Route::delete('/admin/penyakit/{kd_penyakit}', 'Admin\PenyakitController@destroy');