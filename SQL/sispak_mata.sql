-- phpMyAdmin SQL Dump
-- version 4.5.4.1deb2ubuntu2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: 27 Nov 2017 pada 00.37
-- Versi Server: 5.7.20-0ubuntu0.16.04.1
-- PHP Version: 7.0.22-0ubuntu0.16.04.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `sispak_mata`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `diagnosa`
--

CREATE TABLE `diagnosa` (
  `kd_diagnosa` int(11) NOT NULL,
  `status_diagnosa` varchar(50) NOT NULL,
  `val_score` float NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `gejala`
--

CREATE TABLE `gejala` (
  `kd_gejala` char(4) NOT NULL,
  `nm_gejala` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `gejala`
--

INSERT INTO `gejala` (`kd_gejala`, `nm_gejala`) VALUES
('AED', 'Veum Green'),
('AL', 'Sporer Radial'),
('ar', 'Sonny Streets'),
('AWG', 'Quigley Mission'),
('BND', 'Kozey Station'),
('bo', 'Fleta Meadow'),
('BZ', 'Rath Shoals'),
('ca', 'Devonte Parks'),
('CRC', 'Price Parkway'),
('CVE', 'Gerlach Field'),
('CX', 'Elvis Lodge'),
('da', 'Shanahan Run'),
('DK', 'Graham Cape'),
('DOP', 'Geovanni Valleys'),
('et', 'Halvorson Haven'),
('EUR', 'Rosemarie Crossing'),
('FJD', 'Stroman Viaduct'),
('GN', 'Sonya Harbor'),
('GU', 'Nannie Summit'),
('GYD', 'Gregg Turnpike'),
('HN', 'Jo Terrace'),
('ii', 'Marquardt Springs'),
('IQD', 'Olson Street'),
('KES', 'Boyle Trafficway'),
('km', 'Kaycee Turnpike'),
('kn', 'Cloyd Stream'),
('ko', 'Nicolas Ridges'),
('KY', 'Rath Haven'),
('KYD', 'Regan Courts'),
('LAK', 'Vickie Parkways'),
('lb', 'Sidney Mission'),
('li', 'Millie Island'),
('MAD', 'Von Plaza'),
('MH', 'Lucas Throughway'),
('mi', 'Roob Fords'),
('MNT', 'Jones Crescent'),
('ng', 'Wintheiser Pike'),
('NPR', 'Webster Spring'),
('OMR', 'Landen Junction'),
('PGK', 'Morissette Way'),
('QAR', 'Carlos Radial'),
('RSD', 'Maxie Via'),
('sg', 'Jacobs Drives'),
('sl', 'Jadyn Green'),
('SOS', 'Mariela Ranch'),
('SZL', 'Stracke Estates'),
('te', 'Murphy Manors'),
('TT', 'Pagac Radial'),
('TTD', 'Billy Plaza'),
('UAH', 'King Island'),
('UGX', 'Stoltenberg Groves'),
('vo', 'Elliot Fields'),
('VUV', 'Stracke Ranch'),
('WST', 'Reichel Trafficway'),
('XCD', 'Damaris Path');

-- --------------------------------------------------------

--
-- Struktur dari tabel `hasil`
--

CREATE TABLE `hasil` (
  `kd_hasil` int(11) NOT NULL,
  `kd_periksa` int(11) NOT NULL,
  `kd_variabel` int(11) NOT NULL,
  `kd_keanggotaan` int(11) NOT NULL,
  `val_alpha` float NOT NULL,
  `kd_penyakit` char(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `kabupaten`
--

CREATE TABLE `kabupaten` (
  `kd_kabupaten` int(11) NOT NULL,
  `nama_kabupaten` varchar(50) NOT NULL,
  `kota` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `keanggotaan`
--

CREATE TABLE `keanggotaan` (
  `kd_keanggotaan` int(11) NOT NULL,
  `val_batas_bawah` float NOT NULL,
  `val_batas_tengah` float NOT NULL,
  `val_batas_atas` float NOT NULL,
  `keterangan` varchar(50) NOT NULL,
  `kd_variabel` int(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `kecamatan`
--

CREATE TABLE `kecamatan` (
  `kd_kecamatan` int(11) NOT NULL,
  `nama_kecamatan` varchar(50) NOT NULL,
  `kd_kabupaten` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `pasien`
--

CREATE TABLE `pasien` (
  `id_pasien` int(11) NOT NULL,
  `nama_pasien` varchar(50) NOT NULL,
  `tmp_lahir` varchar(50) NOT NULL,
  `tgl_lahir` date NOT NULL,
  `gender` enum('laki-laki','perempuan') NOT NULL,
  `alamat` varchar(50) NOT NULL,
  `kd_kecamatan` int(11) NOT NULL,
  `kd_kabupaten` int(11) NOT NULL,
  `telp` int(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `pemeriksaan`
--

CREATE TABLE `pemeriksaan` (
  `kd_periksa` int(11) NOT NULL,
  `kd_gejala` char(4) NOT NULL,
  `tgl_periksa` date NOT NULL,
  `id_pasien` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `pemeriksaan_detail`
--

CREATE TABLE `pemeriksaan_detail` (
  `kd_pemeriksaan_detail` int(11) NOT NULL,
  `kd_pemeriksaan` int(11) NOT NULL,
  `z_score_total` float NOT NULL,
  `status_periksa` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `penyakit`
--

CREATE TABLE `penyakit` (
  `kd_penyakit` char(4) NOT NULL,
  `nm_penyakit` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `penyakit`
--

INSERT INTO `penyakit` (`kd_penyakit`, `nm_penyakit`) VALUES
('ANG', 'Jennings Trafficway'),
('BOB', 'Conor Lodge'),
('BTN', 'Verlie Corner'),
('GTQ', 'Grimes Trace'),
('HTG', 'Gleichner Fort'),
('IDR', 'Cummings Street'),
('KGS', 'Bosco Lodge'),
('KZT', 'Amparo Meadows'),
('MAD', 'Miller Field'),
('SHP', 'Lind Mission'),
('TTD', 'Ardith Ramp');

-- --------------------------------------------------------

--
-- Struktur dari tabel `users`
--

CREATE TABLE `users` (
  `username` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL,
  `nama` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL,
  `foto` varchar(100) NOT NULL,
  `id_user_level` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `user_level`
--

CREATE TABLE `user_level` (
  `id_user_level` int(11) NOT NULL,
  `level` varchar(25) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `variabel`
--

CREATE TABLE `variabel` (
  `kd_variabel` int(4) NOT NULL,
  `nama_variabel` varchar(50) NOT NULL,
  `srt_num` int(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `diagnosa`
--
ALTER TABLE `diagnosa`
  ADD PRIMARY KEY (`kd_diagnosa`);

--
-- Indexes for table `gejala`
--
ALTER TABLE `gejala`
  ADD PRIMARY KEY (`kd_gejala`);

--
-- Indexes for table `hasil`
--
ALTER TABLE `hasil`
  ADD PRIMARY KEY (`kd_hasil`),
  ADD KEY `kd_periksa` (`kd_periksa`),
  ADD KEY `kd_variabel` (`kd_variabel`),
  ADD KEY `kd_keanggotaan` (`kd_keanggotaan`),
  ADD KEY `kd_penyakit` (`kd_penyakit`);

--
-- Indexes for table `kabupaten`
--
ALTER TABLE `kabupaten`
  ADD PRIMARY KEY (`kd_kabupaten`);

--
-- Indexes for table `keanggotaan`
--
ALTER TABLE `keanggotaan`
  ADD PRIMARY KEY (`kd_keanggotaan`);

--
-- Indexes for table `kecamatan`
--
ALTER TABLE `kecamatan`
  ADD PRIMARY KEY (`kd_kecamatan`),
  ADD KEY `kd_kabupaten` (`kd_kabupaten`);

--
-- Indexes for table `pasien`
--
ALTER TABLE `pasien`
  ADD PRIMARY KEY (`id_pasien`),
  ADD KEY `kd_kecamatan` (`kd_kecamatan`),
  ADD KEY `kd_kabupaten` (`kd_kabupaten`);

--
-- Indexes for table `pemeriksaan`
--
ALTER TABLE `pemeriksaan`
  ADD PRIMARY KEY (`kd_periksa`),
  ADD KEY `kd_gejala` (`kd_gejala`),
  ADD KEY `id_pasien` (`id_pasien`);

--
-- Indexes for table `pemeriksaan_detail`
--
ALTER TABLE `pemeriksaan_detail`
  ADD PRIMARY KEY (`kd_pemeriksaan_detail`),
  ADD KEY `kd_pemeriksaan` (`kd_pemeriksaan`);

--
-- Indexes for table `penyakit`
--
ALTER TABLE `penyakit`
  ADD PRIMARY KEY (`kd_penyakit`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`username`),
  ADD KEY `id_user_level` (`id_user_level`);

--
-- Indexes for table `user_level`
--
ALTER TABLE `user_level`
  ADD PRIMARY KEY (`id_user_level`);

--
-- Indexes for table `variabel`
--
ALTER TABLE `variabel`
  ADD PRIMARY KEY (`kd_variabel`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `hasil`
--
ALTER TABLE `hasil`
  MODIFY `kd_hasil` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `kabupaten`
--
ALTER TABLE `kabupaten`
  MODIFY `kd_kabupaten` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `keanggotaan`
--
ALTER TABLE `keanggotaan`
  MODIFY `kd_keanggotaan` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `kecamatan`
--
ALTER TABLE `kecamatan`
  MODIFY `kd_kecamatan` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `pasien`
--
ALTER TABLE `pasien`
  MODIFY `id_pasien` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `pemeriksaan`
--
ALTER TABLE `pemeriksaan`
  MODIFY `kd_periksa` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `pemeriksaan_detail`
--
ALTER TABLE `pemeriksaan_detail`
  MODIFY `kd_pemeriksaan_detail` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `user_level`
--
ALTER TABLE `user_level`
  MODIFY `id_user_level` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `variabel`
--
ALTER TABLE `variabel`
  MODIFY `kd_variabel` int(4) NOT NULL AUTO_INCREMENT;
--
-- Ketidakleluasaan untuk tabel pelimpahan (Dumped Tables)
--

--
-- Ketidakleluasaan untuk tabel `hasil`
--
ALTER TABLE `hasil`
  ADD CONSTRAINT `hasil_ibfk_1` FOREIGN KEY (`kd_keanggotaan`) REFERENCES `keanggotaan` (`kd_keanggotaan`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `hasil_ibfk_2` FOREIGN KEY (`kd_periksa`) REFERENCES `pemeriksaan` (`kd_periksa`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `hasil_ibfk_3` FOREIGN KEY (`kd_variabel`) REFERENCES `variabel` (`kd_variabel`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `hasil_ibfk_4` FOREIGN KEY (`kd_penyakit`) REFERENCES `penyakit` (`kd_penyakit`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ketidakleluasaan untuk tabel `kecamatan`
--
ALTER TABLE `kecamatan`
  ADD CONSTRAINT `kecamatan_ibfk_1` FOREIGN KEY (`kd_kabupaten`) REFERENCES `kabupaten` (`kd_kabupaten`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ketidakleluasaan untuk tabel `pasien`
--
ALTER TABLE `pasien`
  ADD CONSTRAINT `pasien_ibfk_1` FOREIGN KEY (`kd_kabupaten`) REFERENCES `kabupaten` (`kd_kabupaten`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `pasien_ibfk_2` FOREIGN KEY (`kd_kecamatan`) REFERENCES `kecamatan` (`kd_kecamatan`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ketidakleluasaan untuk tabel `pemeriksaan`
--
ALTER TABLE `pemeriksaan`
  ADD CONSTRAINT `pemeriksaan_ibfk_1` FOREIGN KEY (`kd_gejala`) REFERENCES `gejala` (`kd_gejala`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `pemeriksaan_ibfk_2` FOREIGN KEY (`id_pasien`) REFERENCES `pasien` (`id_pasien`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ketidakleluasaan untuk tabel `pemeriksaan_detail`
--
ALTER TABLE `pemeriksaan_detail`
  ADD CONSTRAINT `pemeriksaan_detail_ibfk_1` FOREIGN KEY (`kd_pemeriksaan`) REFERENCES `pemeriksaan` (`kd_periksa`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ketidakleluasaan untuk tabel `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `users_ibfk_1` FOREIGN KEY (`id_user_level`) REFERENCES `user_level` (`id_user_level`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
