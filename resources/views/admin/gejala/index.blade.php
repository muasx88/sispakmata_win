@extends('admin.layout.master')

@section('konten')

<section class="content-header">
  <h1>
    {{ $title }}
  </h1>
  <ol class="breadcrumb">
    <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
    <li class="active">{{ $title }}</li>
  </ol>
</section>

<!-- Main content -->
<section class="content">

	<div class="box">
		<div class="box-header">
            <a href="#" class="btn btn-success"><i class="fa fa-plus-circle"></i> Tambah</a>
         </div>
		<div class="box-body">
		  <table id="example1" class="table table-bordered table-striped">
		    <thead>
		    <tr>
		      <th>Kode Gejala</th>
		      <th>Gejala</th>
		      <th>Aksi</th>
		    </tr>
		    </thead>
		    <tbody>
		    @foreach ($gejalas as $gejala)
		    <tr>
		    	<td>{{ $gejala->kd_gejala }}</td>
		    	<td>{{ $gejala->nm_gejala }}</td>
		    	<td width="16%">
		    		<a href="#" class="btn btn-primary"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit</a>&nbsp&nbsp
		    		<a href="#" class="btn btn-danger"><i class="fa fa-trash-o" aria-hidden="true"></i> Hapus</a>
		    	</td>
		    </tr>
		    @endforeach
		    </tbody>
		  </table>
		</div>
		<!-- /.box-body -->
	</div>
</section>

@endsection

<!-- jQuery 2.2.3 -->
<script src="{{ url('plugins/jQuery/jquery-2.2.3.min.js') }}"></script>
<!-- Datatables -->
<script src="{{ url('plugins/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ url('plugins/datatables/dataTables.bootstrap.min.js') }}"></script>
<script>
  $(function () {
    $("#example1").DataTable();
    $('#example2').DataTable({
      "paging": true,
      "lengthChange": false,
      "searching": false,
      "ordering": true,
      "info": true,
      "autoWidth": false
    });
  });
</script>