@extends('admin.layout.master')

@section('konten')

<section class="content-header">
  <h1>
    {{ $title }}
    <small>Control panel</small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
    <li class="active">{{ $title }}</li>
  </ol>
</section>

<!-- Main content -->
<section class="content">

	<h1>SISPAKMATA - Sistem Pakar Diagnosa Penyakit Mata</h1>
	<br><br>
	<img src="{{ url('dist/img/red-irritated-eye.jpg') }}" alt="" class="user-image" style="heigt:500px;width:500px;">
</section>

@endsection