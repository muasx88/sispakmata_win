@extends('admin.layout.master')

@section('konten')

<section class="content-header">
  <h1>
    {{ $title }}
  </h1>
  <ol class="breadcrumb">
    <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
    <li class="active">{{ $breadcrumb }}</li>
  </ol>
</section>

<!-- Main content -->
<section class="content">
	<div class="box">
		<div class="box-header">
            <a href="/admin/penyakit/create" class="btn btn-success"><i class="fa fa-plus-circle"></i> Tambah</a>
         </div>
		<div class="box-body">
		  <table id="example1" class="table table-bordered table-striped">
		    <thead>
		    <tr>
		      <th>Kode Penyakit</th>
		      <th>Penyakit</th>
		      <th>Aksi</th>
		    </tr>
		    </thead>
		    <tbody>
		    @foreach ($penyakits as $penyakit)
		    <tr>
		    	<td>{{ $penyakit->kd_penyakit }}</td>
		    	<td>{{ $penyakit->nm_penyakit }}</td>
		    	<td width="16%">
		    		<form action="/admin/penyakit/{{ $penyakit->kd_penyakit }}" method="POST" onsubmit = "return confirm('Are you sure?')">
		    			<input type="hidden" name="_method" value="DELETE">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
		    			<a href="/admin/penyakit/{{ $penyakit->kd_penyakit }}/edit" class="btn btn-primary"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit</a>
		    			<button type="submit" class="btn btn-danger"><i class="fa fa-trash-o"></i>
                          Hapus
                        </button>
					</form>
		    	</td>
		    </tr>
		    @endforeach
		    </tbody>
		  </table>
		</div>
		<!-- /.box-body -->
	</div>

</section>

@endsection

<!-- jQuery 2.2.3 -->
<script src="{{ url('plugins/jQuery/jquery-2.2.3.min.js') }}"></script>
<!-- Datatables -->
<script src="{{ url('plugins/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ url('plugins/datatables/dataTables.bootstrap.min.js') }}"></script>
<script>
  $(function () {
    $("#example1").DataTable();
    $('#example2').DataTable({
      "paging": true,
      "lengthChange": false,
      "searching": false,
      "ordering": true,
      "info": true,
      "autoWidth": false
    });
  });
</script>