@extends('admin.layout.master')

@section('konten')


<section class="content-header">
  <h1>
    {{ $title }}
  </h1>
  <ol class="breadcrumb">
    <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
    <li class="active">{{ $breadcrumb }}</li>
  </ol>
</section>

<!-- Main content -->
<section class="content">
	<div class="box">
		<div class="box-body">
				<form action="/admin/penyakit/{{ $penyakit[0]->kd_penyakit }}" method="POST">
					<div class="form-group">
						<label>Kode Penyakit</label>
						<input type="text" name="kd_penyakit" class="form-control" maxlength="4" value="{{ $penyakit[0]->kd_penyakit }}" readonly>
					</div>
					<div class="form-group">
						<label>Penyakit</label>
						<input type="text" name="penyakit" class="form-control" value="{{ $penyakit[0]->nm_penyakit }}">
					</div>
					<input type="submit" name="submit" class="btn btn-primary" value="Update">
					{{ csrf_field() }}
					<input type="hidden" name="_method" value="PUT">
					
				</form>
		</div>
		<!-- /.box-body -->
	</div>

</section>

@endsection