<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

use Illuminate\Http\Request;

use App\Penyakit;

class PenyakitController extends Controller
{
    
    public function index()
    {
        $title = 'Data Penyakit';
        $breadcrumb = 'Penyakit';

        $penyakits = DB::table('penyakit')->get();
        return view('admin.penyakit.index',['title'=>$title, 'penyakits'=>$penyakits, 'breadcrumb'=>$breadcrumb]);
    }

    public function create()
    {
        $title = 'Tambah Penyakit';
        $breadcrumb = 'Penyakit';

        return view('admin.penyakit.create',['breadcrumb'=>$breadcrumb, 'title'=>$title]);
    }

    public function store(Request $request)
    {
        $penyakit = new Penyakit();
        $penyakit->kd_penyakit = $request->input('kd_penyakit');
        $penyakit->nm_penyakit = $request->input('penyakit');
        $penyakit->save();

        return redirect('admin/penyakit');
    }

    public function show($id)
    {
        //
    }

    public function edit($kd_penyakit)
    {
        $title = 'Edit Penyakit';
        $breadcrumb = 'Penyakit';

        $penyakit = DB::table('penyakit')->where('kd_penyakit',$kd_penyakit)->get();
        // dd($penyakit);
        return view('admin.penyakit.edit',['penyakit'=>$penyakit,'breadcrumb'=>$breadcrumb, 'title'=>$title]);
    }

    public function update(Request $request, $kd_penyakit)
    {
        // dd($request);
        // $penyakit = Penyakit::where('kd_penyakit', $kd_penyakit)->first();
        // dd($penyakit);
        // $penyakit->nm_penyakit = $request->input('penyakit');
        // $penyakit->save();

        $penyakit = DB::table('penyakit')->where('kd_penyakit',$kd_penyakit)
                                        ->update(['nm_penyakit'=> $request->penyakit] );
        return redirect('admin/penyakit');
    }

    public function destroy($kd_penyakit)
    {
        $penyakit = DB::table('penyakit')->where('kd_penyakit',$kd_penyakit)->delete();
        return redirect('admin/penyakit');
    }
}
