<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Penyakit extends Model
{
    protected $table='penyakit';
    public $timestamps = true;
    // protected $casts = ['id' => 'string'];
}
